<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('b_name');
            $table->string('b_lastname')->nullable();
            $table->string('g_name');
            $table->string('g_lastname')->nullable();
            $table->dateTime('date');
            $table->string('w_venue', 1000)->nullable();
            $table->string('r_venue', 1000)->nullable();
            $table->string('story', 1000)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
