<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get ( '/redirect/{service}', 'Auth\SocialAuthController@redirect' );
Route::get ( '/callback/{service}', 'Auth\SocialAuthController@callback' );

//Route::get('/home', 'HomeController@index')->name('home');
Route::post('registration', 'Auth\RegisterController@create')->name('registration');
Route::get('logout', 'Auth\LoginController@logout');
Route::post('choose', 'User\OrderController@choosePlan')->name('plan.choose');
Route::get('choose_Design', 'User\OrderController@showChoosePage')->name('designPage');
Route::post('choose_Design', 'User\OrderController@chooseDesign')->name('design.choose');
Route::post('/pay', 'PaymentController@redirectToGateway')->name('pay');
Route::get('/pay', 'PaymentController@handleGatewayCallback')->name('pay');

Route::group(['prefix'=>'dashboard', 'middleware'=>['auth']], function(){
    Route::get('/', 'User\DashboardController@index');

    /*domain registration*/
    Route::get('domain', 'User\ProfileController@domain')->name('domain');
    Route::post('freeDomain', 'User\ProfileController@freeDomain')->name('freeDomain');
    Route::post('check', 'User\ProfileController@check')->name('check');

    /*profile*/
    Route::get('profile', 'User\ProfileController@showForm')->name('profile');
    Route::post('saveProfile', 'User\ProfileController@saveDetails')->name('saveDetail');
    Route::get('orders', 'User\OrderController@order')->name('order');
    Route::delete('orders/{id}', 'User\OrderController@deleteOrder')->name('deleteOrder');
    Route::get('receipt', 'PaymentController@Receipt');

    /*image uploads*/
    Route::get('upload', 'UploadImagesController@create')->name('uploadImage');
    Route::post('/images-save', 'UploadImagesController@store')->name('saveImage');
    Route::delete('/images-delete', 'UploadImagesController@destroy')->name('deleteImage');
    Route::get('/images-show', 'UploadImagesController@index');

    /*gallery*/
    Route::get('gallery', 'GalleryController@index')->name('gallery');
});
Route::group(['prefix'=>'admin/dashboard', 'middleware'=>['admin','auth']], function(){
    Route::get('/', 'Admin\DashboardController@index')->name('admin');

    Route::get('plans', 'Admin\PlansController@index')->name('plans.index');
    Route::post('plans', 'Admin\PlansController@savePlan')->name('savePlan');
    Route::delete('plans/{id}', 'Admin\PlansController@deletePlan')->name('deletePlan');
    Route::delete('plans/per/{id}', 'Admin\PlansController@destroyPlan')->name('destroyPlan');
    Route::post('plans/restore/{id}', 'Admin\PlansController@restorePlan')->name('restorePlan');

    Route::get('designs', 'DesignController@index')->name('designs.index');
    Route::post('designs', 'DesignController@save')->name('saveDesign');
    Route::delete('designs/{id}', 'DesignController@delete')->name('deleteDesign');
    Route::delete('designs/perm/{id}', 'DesignController@destroy')->name('destroyDesign');
    Route::post('designs/restore/{id}', 'DesignController@restore')->name('restoreDesign');

    Route::get('orders', 'User\OrderController@AdminIndex')->name('orders.index');
    Route::get('users', 'User\UserController@index')->name('users.index');
});

/*gift registry route*/
Route::group(['prefix'=>'gift-registry'], function(){
    Route::get('/', 'Gift\GiftRegistryController@index')->name('giftRegistry')->middleware('checkToken');
});



