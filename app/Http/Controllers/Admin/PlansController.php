<?php

namespace App\Http\Controllers\Admin;

use App\Plan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlansController extends Controller
{
    public function index()
    {
        if(\request()->has('trash')){
            $plans = Plan::onlyTrashed()->get();
        }else{
            $plans= Plan::all();
        }
        return view('admin.plans', compact('plans'));
    }

    /**
     * this method saves and edit a plan
     * @param Request $request for passed datas from the user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function savePlan(Request $request)
    {
        if ($request->has('id'))
        {
            $plan= Plan::find($request->id);
        }else{
            $plan = new Plan();
        }
        $plan->name = $request->name;
        $plan->price = $request->price;
        $plan->description = $request->description;
        $plan->save();
        return back()->with('status', 'Plan Added Successfully!');
    }

    /**
     * this method temporaryly deletes a plan
     * @param $id is the plan id passed through the route
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deletePlan($id)
    {
        $plan= Plan::findOrFail($id);
        $plan->delete();
        return back()->with('status', 'Successfully moved to Trash!');
    }


    /**
     * this method permanently deletes a plan
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyPlan($id)
    {
        $plan= Plan::onlyTrashed()->findOrFail($id);
        $plan->forceDelete();
        return back()->with('status', 'Deleted Successfully!');
    }


    /**
     * this method restores a temporaryly deleted plan
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restorePlan($id)
    {
        $plan= Plan::onlyTrashed()->findOrFail($id);
        $plan->forceDelete();
        return back()->with('status', 'Restored Successfully!');
    }


}
