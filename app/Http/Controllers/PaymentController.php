<?php

namespace App\Http\Controllers;

use App\Order;
use App\Payment;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Paystack;

class PaymentController extends Controller
{

    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        return Paystack::getAuthorizationUrl()->redirectNow();
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();

//        dd($paymentDetails);
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
        $payment= new Payment();
        $payment->user_id= Auth::id();
        $payment->order_id= array_get($paymentDetails, 'data.metadata.order_id');
        $payment->plan_id= array_get($paymentDetails, 'data.metadata.plan_id');
        $payment->transaction_id= array_get($paymentDetails, 'data.id');
        $payment->invoice= rand(1,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
        $payment->reference= array_get($paymentDetails, 'data.reference');
        $payment->amount= array_get($paymentDetails, 'data.amount');
        $payment->paidAt= array_get($paymentDetails, 'data.paid_at');
        $payment->gateway= array_get($paymentDetails, 'data.channel');
        $payment->save();

        //update Payment
        $order = Order::find($payment->order_id);
        $order->status = 2;
        $order->save();

        return redirect()->route('domain')->with('status', 'Payment Successful!');
//        return redirect('/dashboard/receipt?receipt='.$payment->id);

    }

    public function receipt()
    {
        $invoice= Payment::where('id', request('receipt'))->where('user_id', Auth::id())->first();
        return view('user.receipt', compact('invoice'));
//        return $invoice;
    }

    /*public function downloadPDF($id){
        $invoices= Payment::where('id', $id)->where('user_id', Auth::id())->get();

        $pdf = PDF::loadView('pdf', compact('invoices'));
        return $pdf->download('invoice.pdf');

    }*/
}