<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
class SocialAuthController extends Controller
{
    public function redirect($service) {
        return Socialite::driver ( $service )->redirect ();
    }

    public function callback($provider) {
        $userSC = Socialite::driver($provider)->user();
        //$SCname = $userSC->name;
        //$SCemail = $userSC->email;
        //$SCtoken = $userSC->token;
        // OAuth Two Providers
        $token = $userSC->token;
        $refreshToken = $userSC->refreshToken; // not always provided
        $expiresIn = $userSC->expiresIn;

        // OAuth One Providers
        //$token = $user->token;
        //$tokenSecret = $userSC->tokenSecret;

        // All Providers
        $SCUserID = $userSC->getId();
        $SCNickName = $userSC->getNickname();
        $SCName = $userSC->getName();
        $SCEmail = $userSC->getEmail();
        $SCAvatar = $userSC->getAvatar();

        $findUser = User::where('email', $SCEmail)->first();

        if($findUser){
            //Auth::login($findUser);
            auth()->login($findUser);
            return redirect("/");

        }else{
            $fullname = explode(" ",$SCName);
            if(count($fullname) > 1){
                $fname = $fullname[0];
                $lastname = $fullname[1];
            }
            $user = new User;
            $user->name = $SCName;
            $user->lastname = $SCName;
            $user->email = $SCEmail;
            $user->uname = $SCNickName;
            $user->password = bcrypt($SCUserID);
            $user->provider = $provider;
            $user->provider_id = $SCUserID;
            if($provider !== 'google'){
                $user->$provider = "http://{$provider}.com/$SCNickName";
            }
            $user->save();

            //Auth::login($user);
            auth()->login($user);

            return redirect("/");


        }

        $userSC->token;
    }
}
