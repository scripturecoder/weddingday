<?php

namespace App\Http\Controllers;

use App\Design;
use Illuminate\Http\Request;

class DesignController extends Controller
{
    public function index()
    {
        $designs= Design::all();
        return view('admin.design', compact('designs'));
    }

    /**
     * this method saves a design
     */
    public function save(Request $request)
    {
        if ($request->has('id'))
        {
            $design= Design::find($request->id);
        }else{
            $design= new Design();
        }
        //get the image and move to the right destination
        $image = $request->image;
        if ($image) {
            $imageName = str_random(20).'.'.$image->getClientOriginalExtension();
            $image->move('images/designs', $imageName);
            $design->photo = $imageName;
        }

        $design->save();
        return back()->with('status', 'Save Successfully!');
    }

    public function edit()
    {

    }

    /**
     * this method temporarily deletes a design
     */
    public function delete($id)
    {
        $design= Design::findOrFail($id);
        $design->delete();
        return back()->with('status', 'Moved to Trash!');
    }

    /**
     * this method permanently delete a design
     */
    public function destroy($id)
    {
        $design= Design::onlyTrashed()->findOrFail($id);
        $design->forceDelete();
        return back()->with('status', 'Deleted Successfully!');
    }

    /**
     * this method restores a temporarily deleted design
     */
    public function restore($id)
    {
        $design= Design::onlyTrashed()->findOrFail($id);
        $design->forceDelete();
        return back()->with('status', 'Restored Successfully!');
    }
}
