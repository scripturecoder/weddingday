<?php

namespace App\Http\Controllers\User;

use App\Design;
use App\Order;
use App\UserDesign;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function choosePlan(Request $request)
    {
        /*store data in session*/
        session(['plan' => $request->plan]);
        return redirect()->route('designPage');
    }

    public function showChoosePage()
    {
        session(['url.intended' => route('designPage')]);
        $designs= Design::all();
        return view('design', compact('designs'));
    }


    public function chooseDesign(Request $request)
    {
        /*store data in session*/
        session(['design' => $request->design]);
        return $this->confirm();
    }

    /** this method saves the order in the database
     * @return \Illuminate\Http\RedirectResponse
     */
    public function confirm()
    {
        /*retrieve session data*/
        $plan = session('plan');
        $design = session('design');

        /*delete previous order to avoid conflict*/
        Order::where('user_id', Auth::id())->where('status', 1)->delete();
        /*store session data*/
        $store= new Order();
        $store->user_id = Auth::id();
        $store->plan_id = $plan;
        $store->design_id = $design;
        $store->status = 1;
        $store->save();

        if ($plan== 1 || $plan==3)
        {
            return redirect()->route('domain');
        }
        return redirect()->route('order');
    }


    public function order()
    {
        $orders= Order::where('user_id', Auth::id())->where('status', 1)->where('plan_id', 2)->get();
        return view('user.confirm', compact('orders'));
    }


    public function deleteOrder($id)
    {
        $order= Order::find($id);
        $order->delete();
        return back();
    }


    /**
     * this return all order in the admin view
     * @return admin order
     */
    public function adminIndex()
    {
        $orders= Order::all();
        return view('admin.orders', compact('orders'));
    }




}
