<?php

namespace App\Http\Controllers\User;

use App\Domain;
use App\Http\Controllers\Controller;
use App\Http\Requests\DomainRequest;
use App\Order;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    public function domain()
    {
        $plan= Order::where('user_id', Auth::id())->value('plan_id');
        if ($plan==1)
        {
            return view('user.freeDomain');
        }
        return view('user.domain');
    }

    public function freeDomain(DomainRequest $request)
    {
        $domain    = $request->domain.'.'.'weddingday.com';
        if (Domain::where('user_id', Auth::id())->exists())
        {
            return back()->with('error', 'You already have an existing domain, kindly contact the admin for complaint');
        }
        if (Domain::where('name',$domain)->exists())
        {
            return back()->with('error', $domain.' '.'is not available, kindly search for another one');
        }

        $save= new Domain();
        $save->user_id= Auth::id();
        $save->name= $domain;
        $save->save();
        return back()->with('status', $domain.' '.'is available and saved successfully, Kindly click next to continue ');
    }

    /**
     * this method search for the domain availability
     */

    public function check(DomainRequest $request)
    {
        if (Domain::where('user_id', Auth::id())->exists())
        {
            return back()->with('error', 'You already have an existing domain, kindly contact the admin for complaint');
        }
        // prepare vars
        $domain    = $request->domain.'.com';     // domain to check
        $r         = "taken";             // request type: domain availability
        $apikey    = "86006209ae53c140d6f426d434d59168";        // your API key

        // API call
        $response = json_decode(file_get_contents(
            "http://api.whoapi.com/?domain=$domain&r=$r&apikey=$apikey"), true);
        if($response['status'] == 0){
            // show the result
            if ($response['taken'] == 0)
            {
                $save= new Domain();
                $save->user_id= Auth::id();
                $save->name= $domain;
                $save->save();
                return back()->with('status', $domain.' '.'is available and saved successfully, Kindly click next to continue ');
            }else{
                return back()->with('error', $domain.' '.'is not available, kindly search for another one');
            }
        }else{
            // show error
            return back()->with('error', 'Unable to check domain availability');
        }
    }


    public function showForm()
    {
        $data= Profile::where('user_id', Auth::id())->exists();
        if ($data)
        {
            return $this->editForm();
        }
        return view('user.form');
    }

    public function editForm()
    {
        $data= Profile::where('user_id', Auth::id())->first();

        return view('user.edit', compact('data'));
    }

    public function saveDetails(Request $request)
    {
        if ($request->has('id'))
        {
            $detail= Profile::find($request->id);
        }else{
            $detail= new Profile();
        }
        $detail->user_id= Auth::id();
        $detail->b_name= $request->b_name;
        $detail->b_lastname= $request->b_lastname;
        $detail->g_name= $request->g_name;
        $detail->g_lastname= $request->g_lastname;
        $detail->date= $request->date;
        $detail->w_venue= $request->w_venue;
        $detail->r_venue= $request->r_venue;
        $detail->story= $request->story;
        $detail->save();

        return redirect('/dashboard')->with('status', 'Save Successfully!');
    }

    public function pictures(Request $request)
    {
        //get the image and move to the right destination
        $image = $request->image;
        if ($image) {
            $imageName = str_random(20).'.'.$image->getClientOriginalExtension();
            $image->move('images/designs', $imageName);
//            $design->photo = $imageName;
        }
    }
}
