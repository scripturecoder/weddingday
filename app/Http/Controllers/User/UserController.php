<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users= User::all();
        return view('admin.users', compact('users'));
    }

    public function editUser()
    {

    }

    public function deleteUser()
    {

    }

    public function restoreUser()
    {

    }

    public function destroyUser()
    {

    }

}

