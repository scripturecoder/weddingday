<?php

namespace App\Http\Controllers;

use App\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GalleryController extends Controller
{
    public function index()
    {
        $uploads= Upload::where('user_id', Auth::id())->paginate(9);
        return view('user.gallery', compact('uploads'));
    }
}
