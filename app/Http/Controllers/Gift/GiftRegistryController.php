<?php

namespace App\Http\Controllers\Gift;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GiftRegistryController extends Controller
{
    public function index()
    {
        session(['token' => \request('token')]);
        return view('gift.index');
    }
}
