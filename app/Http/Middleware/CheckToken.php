<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token= $request->token;
        if($request->has('token')){
            if (User::where('token', $token)->exists()) {
                return $next($request);
            }
        }
        return redirect('/');
    }
}
