<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $dates= ['date'];
    public function user()
    {
        $this->belongsTo(User::class);
    }
}
