@extends('user.layout.app')
@section('content')
    <div class="container w3-card w3-margin-top" style="max-width: 500px; background: white">
        <h4 class="w3-margin-top w3-center">Edit Your Wedding Site Information</h4>
        <form action="{{route('saveDetail')}}" method="POST" class="w3-margin-top w3-margin-bottom">
            {{ csrf_field() }}
            <label class="w3-margin-top">Bride's First Name</label>
            <input type="text" name="b_name" value="{{$data->b_name}}" class="w3-input w3-border">

            <label class="w3-margin-top">Bride's Last Name</label>
            <input type="text" name="b_lastname" value="{{$data->b_lastname}}"  class="w3-input w3-border">

            <label class="w3-margin-top">Groom's First Name</label>
            <input type="text" name="g_name" value="{{$data->g_name}}"  class="w3-input w3-border">

            <label class="w3-margin-top">Groom's Last Name</label>
            <input type="text" name="g_lastname" value="{{$data->g_lastname}}"  class="w3-input w3-border">

            <label class="w3-margin-top">Date({{$data->date->diffForHumans()}})</label>
            <input type="datetime-local" class="w3-input w3-border" value="{{$data->date}}"  name="date">

            <label class="w3-margin-top" >Wedding Venue</label>
            <textarea name="w_venue" class="w3-input w3-border">{{$data->w_venue}}</textarea>

            <label class="w3-margin-top">Reception Venue</label>
            <textarea name="r_venue" class="w3-input w3-border">{{$data->r_venue}}</textarea>

            <label class="w3-margin-top">Wedding story</label>
            <textarea name="story" class="w3-input w3-border">{{$data->story}}</textarea>

            <button class="btn btn-primary w3-margin-top w3-margin-bottom w3-right">Update Now</button>
        </form><br>

    </div>
@endsection