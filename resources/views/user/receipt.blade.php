@extends('user.layout.app')
@section('content')
<section>

    <div id="printablediv" class="container w3-border w3-padding-large w3-responsive" style="max-width: 750px; margin-top: 40px; background-color: whitesmoke; ">
        <h5 class="w3-border w3-padding-large w3-center" style="letter-spacing: 15px; background-color: #4a4a4a; color: white; margin-top: 40px;">RECEIPT</h5>
        <div class="w3-rol">
            <div class="w3-half w3-margin-top">
                <p>{{\App\User::where('id', $invoice->user_id)->value('name', 'lastname')}},<br>
                    {{\App\User::where('id', $invoice->user_id)->value('email')}}
                </p>
            </div>
            <div class="w3-half">
                <table class="table w3-bordered w3-border w3-margin-top">
                    <tr>
                        <th>Receipt#</th>
                        <td>{{$invoice->invoice}}</td>
                    </tr>
                    <tr>
                        <th>Date</th>
                        <td>{{$invoice->created_at->format('F j, Y')}}</td>
                    </tr>
                    <tr>
                        <th>Amount Due</th>
                        <td>&#8358; {{number_format($invoice->amount/100,2)}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <table class="table w3-bordered w3-border w3-margin-top">
            <tr>
                <th colspan="2" >&nbsp&nbsp&nbsp&nbspPlan</th>
                <th class="w3-right">Amount&nbsp&nbsp&nbsp&nbsp</th>
            </tr>
            <tr>
                <td colspan="2" >&nbsp&nbsp&nbsp&nbsp{{\App\Plan::where('id', $invoice->plan_id)->value('name')}}</td>
                <td class="w3-right">&#8358; {{number_format($invoice->amount,2)}}&nbsp&nbsp&nbsp&nbsp</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td class="w3-right"><strong>Sub Total:</strong>&nbsp&nbsp&nbsp&#8358; {{number_format($invoice->amount/100,2)}}&nbsp&nbsp&nbsp&nbsp</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td class="w3-right"><strong>Total:</strong>&nbsp&nbsp&nbsp&#8358; {{number_format($invoice->amount/100,2)}}&nbsp&nbsp&nbsp&nbsp</td>
            </tr>
        </table>

        <table class="table w3-bordered w3-border w3-margin-top w3-centered">
            <tr class="w3-centered">
                <th>Transaction Date</th>
                <th>Gateway</th>
                <th>Transaction ID</th>
                <th>Amount</th>
            </tr>
            <tr>
                <td>{{$invoice->created_at->format('F j, Y')}}</td>
                <td>{{$invoice->gateway}}</td>
                <td>{{$invoice->transaction_id}}</td>
                <td>&#8358; {{number_format($invoice->amount/100,2)}}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><strong>Balance</strong> &nbsp&nbsp&nbsp&nbsp  &#8358;{{number_format($invoice->amount/100,2)}}</td>
            </tr>
        </table>
        <div class="w3-margin-top w3-right" style="margin-bottom: 40px;">
            <button class="w3-button w3-blue"><i class="fa fa-print" name="b_print"  onclick="javascript:printerDiv('printablediv')" ></i> Print</button>
{{--            <a href="{{route('pdf', $invoice->id)}}" class="w3-button w3-blue"><i class="fa fa-download"> Download</i></a>--}}
        </div>
    </div>
</section>
<script>
    function printerDiv(divID) {
//Get the HTML of div

        var divElements = document.getElementById(divID).innerHTML;

//Get the HTML of whole page
        var oldPage = document.body.innerHTML;

//Reset the pages HTML with divs HTML only

        document.body.innerHTML =

            "<html><head><title></title></head><body>" +
            divElements + "</body>";



//Print Page
        window.print();

//Restore orignal HTML
        document.body.innerHTML = oldPage;

    }
</script>
@endsection

