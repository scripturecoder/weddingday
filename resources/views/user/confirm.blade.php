@extends('user.layout.app')
@section('content')
    <div style="margin-top: 10%">
        <div class="container w3-card w3-margin-top" style="max-width: 550px; background: white;">
            <h4 class="w3-margin-top w3-center">Confirm Your Order</h4>
            <table class="w3-table w3-stripe w3-bordered">
                <tr>
                    <th></th>
                    <th>Plan</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>
                @forelse($orders as $order)
                    @php
                        $plan= \App\Plan::where('id', $order->plan_id);
                    @endphp
                    <tr>
                        <td></td>
                        <td>{{$plan->value('name')}}</td>
                        <td>&#8358;{{number_format($plan->value('price'),2)}}</td>
                        <td>
                            <form style="display: inline-block" method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                                <input type="hidden" name="email" value="{{Auth::user()->email}}"> {{-- required --}}
                                <input type="hidden" name="orderID" value="{{$order->plan_id}}">
                                <input type="hidden" name="amount" value="{{$plan->value('price') * 100}}"> {{-- required in kobo --}}
                                <input type="hidden" name="metadata" value="{{ json_encode($array = ['order_id' => $order->id, 'plan_id' => $order->plan_id,]) }}" > {{-- For other necessary things you want to add to your payload. it is optional though --}}
                                <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
                                <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> {{-- required --}}
                                {{ csrf_field() }} {{-- works only when using laravel 5.1, 5.2 --}}
                                <button class="btn btn-success">Pay</button>
                            </form>
                            <form style="display: inline-block" action="{{route('deleteOrder', $order->id)}}" method="post">
                                {{csrf_field()}}
                                {{ method_field('delete') }}
                                <button class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td  class="w3-center" colspan="4"> No Order!</td>
                    </tr>
                @endforelse
            </table>

        </div>
    </div>
@endsection