@extends('user.layout.app')
@section('style')
    <link rel="stylesheet" href="{{asset('css/button.css')}}">
@endsection
@section('content')
    <div class="container w3-margin-top w3-card-2" style="max-width: 500px; min-height: 500px; background: #2d222291;">
        <div class="icons w3-margin-top w3-center">
            <a class="button" href="{{route('profile')}}"><span><i class="fa fa-code"></i></span>Edit Wedding website</a><br>
            <a class="button" href="{{route('uploadImage')}}"><span><i class="fa fa-upload"></i></span>Upload Pictures</a><br>
            <a class="button" href="{{route('gallery')}}"><span><i class="fa fa-camera"></i></span>Gallery</a><br>
            <a class="button" href="{{route('profile')}}"><span><i class="fa fa-book"></i></span>Guest Book</a><br>
            <a class="button" href="{{route('profile')}}"><span><i class="fa fa-gift"></i></span>Gift Registry</a><br>
            <a class="button" href="{{route('profile')}}"><span><i class="fa fa-lock"></i></span>Change Password</a>
        </div>
    </div>
@endsection