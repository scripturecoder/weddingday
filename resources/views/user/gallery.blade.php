@extends('user.layout.app')
@section('title', '')
@section('content')
    <section>
        <div class="container" style="max-width: 800px">
            <div class="w3-container w3-padding-16" style="background: #ffffff;">
                <div class="w3-row-padding  w3-animate-zoom">
                    @forelse($uploads as $upload)
                    <div class="w3-third w3-margin-bottom w3-display-container">
                        <img id="img" class="w3-hover-opacity" onclick="onClick(this)" src="{{asset('images/uploads/'.$upload->filename)}}" alt="Image" style="width:100%; height: 250px; cursor: pointer; border: dotted red">
                    </div>
                        @empty
                        No image in gallery, click <a href="{{route('uploadImage')}}">here</a> to upload
                    @endforelse
                </div>
                <div align="center">
                    {{$uploads->links()}}
                </div>
            </div>
        </div>

        <div id="id01" class="w3-modal" onclick="this.style.display='none'">
            <div class="w3-modal-content w3-animate-zoom" >
                <div class="">
                    <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                    <img class="w3-modal-content" id="img01" style="width:100%">
                    <div class="w3-display-middle w3-xlarge">
                        <form method="post" action="{{route('deleteImage')}}" onsubmit="return confirm('Are you sure you want to delete this image?');">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <input type="hidden" name="id" value="{{$upload->id}}">
                            <button class="w3-button w3-red w3-display-middle">Delete</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <script>
            function onClick(element) {
                document.getElementById("img01").src = element.src;
                document.getElementById("id01").style.display = "block";
            }
        </script>
    </section>
@endsection