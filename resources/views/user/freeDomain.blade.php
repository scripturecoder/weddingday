@extends('user.layout.app')
@section('title', '')
@section('content')
    @php
        $dom=\App\Domain::where('user_id', Auth::id())->value('name');
        $domain= explode(".",$dom)
    @endphp
    <section>
        <div class="container" style="max-width: 550px; margin-top: 10%;">
            <div class="w3-container w3-card w3-padding-64" style="background: white">
                <label>Choose a domain name</label><p style="display: inline-block">(input without extension)</p>
                <form method="post" action="{{route('freeDomain')}}">
                    {{ csrf_field() }}
                    <div class="w3-row">
                        <div class="w3-col" style="width: 70%">
                            <input value="@if($dom){{$domain[0]}}@else {{old('domain')}} @endif" name="domain" class="w3-input w3-border" type="text"  required>
                        </div>
                        <div class="w3-col" style="width: 30%">
                            <input type="text" disabled class="w3-input w3-border" value=".weddingday.com">
                        </div>
                    </div>
                    <div class="w3-margin-top">
                        <button class="btn w3-blue">Check Availability</button>
                    </div>
                </form>
                <footer  style="background: #ffffff;" >
                    <form class="w3-margin-top w3-right" action="{{route('profile')}}">
                        <button class="btn w3-green" @if(!$dom) disabled @endif>Next</button>
                    </form>
                </footer>

            </div>
        </div>
    </section>
@endsection