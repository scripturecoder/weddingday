@extends('admin.layouts.app')
@section('content')
    <div class="w3-panel">
        <h3 style="display: inline-block">Orders</h3>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>User</th>
                <th>Name</th>
                <th>Price</th>
                <th>Design</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @php
                $counter= 1;
            @endphp
            @foreach($orders as $order)
                @php
                $user=\App\User::where('id', $order->user_id)->first();
                @endphp
                <tr>
                    <th>{{$counter++}}</th>
                    <td>{{$user->name.' '.$user->lastname}}</td>
                    <td>{{\App\Plan::where('id', $order->plan_id)->value('name')}}</td>
                    <td>{{\App\Plan::where('id', $order->plan_id)->value('price')}}</td>
                    <td>{{$order->design_id}}</td>
                    <td>{{\App\Status::where('id', $order->status)->value('name')}}</td>
                    <td>
                        <div class="btn-group">
                            <button class="btn w3-blue">View</button>
                            <button class="btn w3-green" data-toggle="modal" data-target="#{{$order->id}}">Edit</button>
                            <form style="display: inline-block"  action="{{route('deleteOrder', $order->id)}}" method="POST" onsubmit="return confirm('Are you sure?');">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <button class="btn btn-danger">Delete</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection