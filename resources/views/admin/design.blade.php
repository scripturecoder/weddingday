@extends('admin.layouts.app')
@section('content')
    <div class="w3-panel">
        <h3 style="display: inline-block">Designs</h3> &nbsp <button class="btn w3-green" data-toggle="modal" data-target="#addDesign">Add Design</button>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Image</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @php
                $counter= 1;
            @endphp
            @foreach($designs as $design)
                <tr>
                    <th>{{$counter++}}</th>
                    <td><img id="{{$design->id}}" class="size" src="{{asset('images/designs/'.$design->photo)}}" onclick="onClick(this)" alt="photo"></td>
                    <td>
                        <div class="btn-group">
{{--                            <button onclick="click({{$design->id}})" class="btn w3-blue">View</button>--}}
                            {{--<button class="btn w3-green" data-toggle="modal" data-target="#{{$design->id}}">Edit</button>--}}
                            <form style="display: inline-block"  action="{{route('deleteDesign', $design->id)}}" method="POST" onsubmit="return confirm('Are you sure?');">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <button class="btn btn-danger">Delete</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    @foreach($designs as $design)
        <div class="modal fade" id="{{$design->id}}">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Modal Heading</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        Modal body..
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
    @endforeach

    <div class="modal fade" id="addDesign">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4  style="display: inline-block" class="modal-title">Add Plan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form action="{{route('saveDesign')}}" method="POST" file="true" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Image:</label>
                            <input name="image" type="file" class="w3-input w3-border">
                        </div>
                        <div class="form-group" align="right">
                            <button class="w3-button w3-block w3-green">Submit</button>
                        </div>
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

    <div id="modal01" class="w3-modal" onclick="this.style.display='none'">
        <span class="w3-button w3-hover-red w3-xlarge w3-display-topright">&times;</span>
        <div class="w3-modal-content w3-animate-zoom">
            <img id="img01" style="width:100%">
        </div>
    </div>

    <script>
        function onClick(element) {
            document.getElementById("img01").src = element.src;
            document.getElementById("modal01").style.display = "block";
        }
    </script>

@endsection