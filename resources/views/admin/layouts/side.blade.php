<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="{{route('admin')}}"><i class="fa fa-dashboard fa-fw nav_icon"></i>Dashboard</a>
            </li>
            <li>
                <a href="{{route('users.index')}}"><i class="fa fa-users fa-fw nav_icon"></i>Users</a>
            </li>
            <li>
                <a href="{{route('plans.index')}}"><i class="fa fa-magnet fa-fw nav_icon"></i>Plans</a>
            </li>
            <li>
                <a href="{{route('designs.index')}}"><i class="fa fa-signal fa-fw nav_icon"></i>Designs</a>
            </li>
            <li>
                <a href="{{route('orders.index')}}"><i class="fa fa-credit-card fa-fw nav_icon"></i>Orders</a>
            </li>
            <li>
                <a href="{{url('logout')}}"><i class="fa fa-sign-out fa-fw nav_icon"></i>Logout</a>
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>