@extends('admin.layouts.app')
@section('content')
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @php
            $counter= 1;
            @endphp
            @foreach($users as $user)
            <tr>
                <th>{{$counter++}}</th>
                <td>{{$user->name.' '.$user->lastname}}</td>
                <td>{{$user->phone}}</td>
                <td>{{$user->email}}</td>
                <td>
                    <div class="btn-group">
                        <button class="btn w3-blue">View</button>
                        <button class="btn w3-green" data-toggle="modal" data-target="#{{$user->id}}">Edit</button>
                        <button class="btn btn-danger">Delete</button>
                    </div>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    @foreach($users as $user)
    <div class="modal fade" id="{{$user->id}}">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Modal Heading</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    Modal body..
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    @endforeach

@endsection