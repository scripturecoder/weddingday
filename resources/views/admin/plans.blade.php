@extends('admin.layouts.app')
@section('content')
    <div class="w3-panel">
        <h3 style="display: inline-block">Plans</h3> &nbsp <button class="btn w3-green" data-toggle="modal" data-target="#addPlan">Add Plan</button>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @php
                $counter= 1;
            @endphp
            @foreach($plans as $plan)
                <tr>
                    <th>{{$counter++}}</th>
                    <td>{{$plan->name}}</td>
                    <td>{{$plan->price}}</td>
                    <td>
                        <div class="btn-group">
                            <button class="btn w3-blue">View</button>
                            <button class="btn w3-green" data-toggle="modal" data-target="#{{$plan->id}}">Edit</button>
                            <button class="btn btn-danger">Delete</button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    @foreach($plans as $plan)
        <div class="modal fade" id="{{$plan->id}}">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Modal Heading</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        Modal body..
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
    @endforeach

    <div class="modal fade" id="addPlan">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4  style="display: inline-block" class="modal-title">Add Plan</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form action="{{route('savePlan')}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Name:</label>
                            <input name="name" type="text" class="w3-input w3-border">
                        </div>
                        <div class="form-group">
                            <label>Price:</label>
                            <input name="price" type="text" class="w3-input w3-border">
                        </div>
                        <div class="form-group">
                            <label>Descriptions(separated by comma):</label>
                            <input name="description" type="text" class="w3-input w3-border">
                        </div>
                        <div class="form-group">
                            <button class="btn-success1">Submit</button>
                        </div>


                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

@endsection