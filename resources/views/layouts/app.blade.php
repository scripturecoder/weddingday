<!DOCTYPE html>
<html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<head>
    <title>Login | Weddingday</title>

    <!--meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Weddingday, wedding website, Wedding day" />
    <script type="application/x-javascript">
        addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); }
    </script>
    <!--//meta tags ends here-->

    <!--booststrap-->

    <link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all">
    <!--//booststrap end-->

    <!-- font-awesome icons -->
    <link href="{{asset('assets/css/font-awesome.css')}}" rel="stylesheet">
    <!-- //font-awesome icons -->

    <!-- banner text slider-->

    <!--// banner text slider-->

    <!--jquery-css counter time-->
    <link rel="stylesheet" href="{{asset('assets/css/jquery.countdown.css')}}" />
    <!--//jquery-css counter time-->

    <!--lightbox slider-->
    <link rel="stylesheet" href="{{asset('assets/css/lightbox.css')}}">
    <!-- lightbox slider-->

    <!--stylesheets-->
    <link href="{{asset('assets/css/style2.css')}}" rel='stylesheet' type='text/css' media="all">
    <!--//stylesheets-->
    <link href="//fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('assets/css/w3.css')}}">

</head>

<body style="background: center no-repeat url('images/background1.jpg'); background-size: 100% 100%;">

<div class="header-w3layouts" style="background-color: #1f1e1e;">
    <!-- Navigation -->
    <div class="container">
        <div class="header-bar">
            <nav class="navbar navbar-default">
                <div class="navbar-header navbar-left">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <h1><a class="navbar-brand" href="index.html">Wedding<span>Day</span></a></h1>
                </div>
            </nav>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<section class="w3-padding-large">
    @if (session('status'))
        <div class="alert alert-success">
            <span class="w3-right" onclick="this.parentElement.style.display='none';"><button>&times;</button></span>
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger">
            <span class="w3-right" onclick="this.parentElement.style.display='none';"><button>&times;</button></span>
            {{ session('error') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <span class="w3-right" onclick="this.parentElement.style.display='none';"><button>&times;</button></span>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @yield('content')
</section>
<footer>

    <div class=" copyright text-center">
        <div class="icons">
            <ul>
                <li><a href="#"><span class="fa fa-facebook" style="color: white;"></span></a></li>
                <li><a href="#"><span class="fa fa-twitter" style="color: white;"></span></a></li>
                <li><a href="#"><span class="fa fa-rss" style="color: white;"></span></a></li>
                <li><a href="#"><span class="fa fa-vk" style="color: white;"></span></a></li>
            </ul>
        </div>

        <p  style="color: white;" >  &copy; 2018 Weddingday.com.ng All Rights Reserved </p>

    </div>

</footer>
</body>
</html>