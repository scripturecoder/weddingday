@extends('layouts.app')

@section('content')
    <div class="container w3-card w3-margin-top" style="max-width: 500px;">
        <h4 class="w3-margin-top w3-center">Signup in Few Minutes</h4>
        <form class="w3-margin-top w3-margin-bottom" method="POST" action="{{ route('registration') }}">
            {{ csrf_field() }}
            <label class="w3-margin-top">Full Name	</label>
            <input type="text" name="name" value="{{ old('name') }}"  class="w3-input w3-border">
            <label class="w3-margin-top">Phone	</label>
            <input type="number" name="phone" value="{{ old('phone') }}" class="w3-input w3-border">

            <label class="w3-margin-top">Email</label>
            <input type="email" class="w3-input w3-border" name="email" value="{{ old('email') }}">
            <label class="w3-margin-top" >Password</label>
            <input type="password" class="w3-input w3-border" name="password">
            <label class="w3-margin-top">Confirm Password</label>
            <input type="password" class="w3-input w3-border" name="password_confirmation">
            <button class="btn btn-primary w3-margin-top w3-margin-bottom w3-right">Signup</button>

            <h3>Have an account?<a href="{{url('/login')}}" style="color: blue;">Login</a></h3>
        </form><br>

        <h4 class="w3-center w3-margin-top">OR</h4>

        <div class="icons w3-margin-top w3-margin-bottom w3-center">
            <a href="#"><span class="fa fa-facebook w3-dark-gray w3-hover-black w3-circle" style="font-size: 25px; padding: 10px 15px 10px 15px"></span></a>
            <a href="#"><span class="fa fa-twitter w3-dark-gray w3-hover-black w3-circle" style="font-size: 25px; padding: 10px;"></span></a>
            <a href="#"><span class="fa fa-github w3-dark-gray w3-hover-black w3-circle" style="font-size: 25px; padding: 10px;"></span></a>
            <a href="#"><span class="fa fa-google w3-dark-gray w3-hover-black w3-circle" style="font-size: 25px; padding: 10px;"></span></a>
        </div>
    </div>
{{--<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}
@endsection
