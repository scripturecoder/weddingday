<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--style_sheet-->
    <link href="{{asset('assets/css/auth.css')}}" rel="stylesheet" type="text/css" media="all"/><!--style_sheet-->
    <link href="{{asset('assets/css/font-awesome.css')}}" rel="stylesheet"><!--style_sheet-->
    <link href="//fonts.googleapis.com/css?family=Courgette&amp;subset=latin-ext" rel="stylesheet"><!--online_fonts-->
    <link href="//fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet"><!--online_fonts-->
    <!--stylesheet-->
</head>
<body class="bgimage">
<!-- main -->
<div class="main">
    <!--header-->
    <div class="content">
        <div class="form-body-w3l city" id="login">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <h2>Login</h2>
            <form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="email-w3ls">
                    <label>Your e-mail</label><br>
                    <input type="email" name="email" value="{{ old('email') }}" placeholder="E-MAIL" required="required">
                    <span class="icon1"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                </div>
                <div class="email-w3ls" style="margin-top: 10px">
                    <label>Your Password</label><br>
                    <input class="passwordinput" type="password" name="password" placeholder="PASSWORD" required="required">
                    <span class="icon1"><i class="fa fa-lock" aria-hidden="true"></i></span>
                </div>
                <div class="email-w3ls">
                    <input type="submit" value="Sign In">
                </div>
                <h5 style="margin-top: 10px; color: white">Don't Have an account? &nbsp<a onclick="openCity('signup')" style="color: blue; cursor: pointer"> Sign-up </a></h5>
            </form>
        </div>
        <div class="form-body-w3l city" id="signup"  style="display:none">
            <h2>Sign-Up</h2>
            <form method="post" action="{{ route('registration') }}">
                {{ csrf_field() }}
                <div class="email-w3ls">
                    <label>Fullname</label><br>
                    <input name="name" type="text" value="{{ old('name') }}"  placeholder="" required="required">
                    <span class="icon1"><i class="fa fa-user" aria-hidden="true"></i></span>
                </div>
                <div class="email-w3ls" style="margin-top: 20px">
                    <label>Phone Number</label><br>
                    <input type="number" name="phone" value="{{ old('phone') }}" placeholder="" required="required">
                    <span class="icon1"><i class="fa fa-phone" aria-hidden="true"></i></span>
                </div>
                <div class="email-w3ls" style="margin-top: 20px">
                    <label>Email</label><br>
                    <input type="email" name="email" value="{{ old('email') }}" placeholder="" required="required">
                    <span class="icon1"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                </div>
                <div class="user-text-w3ls">
                    <div class="w3ls-user">
                        <label>Password</label><br>
                        <input class="password" type="password" name="password" placeholder="" required="required">
                        <span class="icon2"><i class="fa fa-lock" aria-hidden="true"></i></span>
                    </div>
                    <div class="w3ls-password">
                        <label>Confirm Password</label><br>
                        <input class="password" type="password" name="password_confirmation" placeholder="" required="required">
                        <span class="icon3"><i class="fa fa-lock" aria-hidden="true"></i></span>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="form-w3l-text">
                    <p>By creating an account you agree to the <a href="#">Terms of Services</a> and <a href="#">Privacy Policy.</a></p>
                </div>
                <input type="submit" value="Sign Up">
                <h5 style="margin-top: 10px; color: white">Already have an account? &nbsp<a onclick="openCity('login')" style="color: blue; cursor: pointer"> Login </a></h5>
            </form>
        </div>
        <div class="line-w3l"><span class="w3l-line"></span>OR<span class="w3l-line"></span></div>
        <div class="socil-icons-agile">
            <a href="redirect/google"><div class="twitter-icon">
                <i class="fa fa-google" aria-hidden="true"></i>
            </div><a/>
            <a href="redirect/facebook"><div class="facebook-icon">
                <i class="fa fa-facebook" aria-hidden="true"></i>
            </div></a>
            <div class="clear"></div>
        </div>
    </div>
    <!--copy-right-->
    <div class="copyright" style="display: none">
        <p>&copy; 2018 Flat Open Account Form. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>
    </div>
    <!--//copy-right-->
</div>
<!--//main-->
<script>
    function openCity(cityName) {
        var i;
        var x = document.getElementsByClassName("city");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        document.getElementById(cityName).style.display = "block";
    }
</script>
</body>
</html>
