<!DOCTYPE html>
<html lang="en">

<head>
    <title>403</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- font files -->
    <link href="//fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Rancho" rel="stylesheet">
    <!-- /font files -->
    <!-- css files -->
    <link href="{{asset('assets/css/403.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('assets/css/font-awesome.css')}}" rel="stylesheet" type="text/css" media="all" />

    <!-- /css files -->

<body>

<div class="container-w3layouts  text-center">

    <h2 class="txt-wthree">403</h2>
    <p>Internal serve error
        <br> Please try again later.</p>
    <div class="home">
        <a href="{{url('/')}}">home</a>
    </div>
</div>
<div class="w3_agile-footer" style="display:none">
    <p> &copy; 2017 Sleet. All Rights Reserved | Design by
        <a href="http://w3layouts.com" target="=_blank">W3layouts</a>
    </p>
</div>
</body>

</html>