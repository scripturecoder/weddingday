
<!DOCTYPE html>
<html>

<head>
    <title>Wedding Day | Home</title>

    <!--meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Weddingday,
wedding website, Wedding day" />
    <script type="application/x-javascript">
        addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); }
    </script>
    <!--//meta tags ends here-->

    <!--booststrap-->

    <link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all">
    <!--//booststrap end-->

    <!-- font-awesome icons -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- for the pricing table -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pricing.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/w3.css')}}">

    <!-- banner text slider-->

    <!--// banner text slider-->

    <!--jquery-css counter time-->
    <link rel="stylesheet" href="{{asset('assets/css/jquery.countdown.css')}}" />
    <!--//jquery-css counter time-->

    <!--lightbox slider-->
    <link rel="stylesheet" href="{{asset('assets/css/lightbox.css')}}">
    <!-- lightbox slider-->

    <!--stylesheets-->
    <link href="{{asset('assets/css/style.css')}}" rel='stylesheet' type='text/css' media="all">
    <!--//stylesheets-->
    <link href="//fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
<div class="preloader"></div>
<div class="banner">

    <div class="header-w3layouts">
        <!-- Navigation -->

        <div class="container">
            <div class="header-bar">
                <nav class="navbar navbar-default">
                    <div class="navbar-header navbar-left">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <h1><a class="navbar-brand" href="{{url('/')}}">Wedding<span>Day</span></a></h1>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                        <nav>
                            <ul class="nav navbar-nav">
                                <li><a href="#about" class="scroll">About</a></li>
                                <li><a href="#services" class="scroll">Services</a></li>
                                <li><a href="#pricing" class="scroll">Pricing</a></li>
                                <li><a href="#contact" class="scroll">Contact</a></li>


                                @if(auth()->check())
                                   {{-- @if (auth()->user()->role === 1)
                                        <a class="w3-bar-item" href="/ddashboard">Dashboard</a>
                                    @elseif (auth()->user()->role === 2)
                                        <a class="w3-bar-item" href="/cdashboard">Dashboard</a>
                                    @elseif (auth()->user()->role === 3)
                                        <a class="w3-bar-item" href="/admin">Dashboard</a>
                                    @endif--}}
                                    <li><a href="/logout">Logout</a></li>
                                    <li><a href="/dashboard">Dashboard</a></li>
                                @else
                                    <li><a href="/login">Signin</a></li>
                                @endif

                            </ul>
                        </nav>



                    </div>
                </nav>
            </div>

            <div class="clearfix"> </div>

        </div>

    </div>
    <div class="container">

        <!--Slider-->
        <div class="slider">
            <div class="callbacks_container w3l">
                <ul class="rslides" id="slider">
                    <li>

                        <div class="slider-info">
                            <h4>Happy Couples For Ever</h4>

                        </div>
                    </li>
                    <li>

                        <div class="slider-info">
                            <h4>The Best Day In Our Lives </h4>
                        </div>
                    </li>
                    <li>

                        <div class="slider-info">
                            <h4>Enjoy Everyday Step of the Journey </h4>
                        </div>
                    </li>
                </ul>

            </div>
            <div class="clearfix"></div>

        </div>

    </div>
</div>
<!--//banner-->
<!--counter-->
<div class="days-coming">
    <div class="container">
        <h3 class="title tittle">The Wedding Day</h3>
        <div class="timer_wrap">
            <div id="counter"></div>
            <div class="clear fix"></div>
        </div>
    </div>
</div>
<!--//counter-->
<!--about-->
<div class="about" id="about">
    <div class="container">
        <h3 class="title">About</h3>
        <div class="about-top-grids">
            <div class="col-md-8 about-top-grid">
                <h4>What Are we
                </h4>
                <p>Welcome to wedding day Nigeria,Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sed odio consequat, tristique elit sed, molestie nulla.
                    Mauris et quam leo. Quisque tincidunt facilisis rutrum. Etiam mattis arcu vitae velit sagittis vehicula. Duis posuere
                    ex in mollis iaculis. Suspendisse tincidunt ut velit id euismod.vulputate turpis porta ex sodales, dignissim hendrerit
                    eros sagittis. Curabitur lacinia dui ut luctus congue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quis
                </p>
                <div class="bar-grids">
                    <h6>Weddings<span> 100% </span></h6>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" style="width: 100%">
                        </div>
                    </div>
                    <div class="mid-bar">
                        <h6>Staff<span> 85% </span></h6>
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped active" style="width: 85%">
                            </div>
                        </div>
                    </div>
                    <h6>Dates<span>67% </span></h6>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" style="width: 67%">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 about-top-image" >
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!--//about-->
<!--footer-->
<div class="buttom-w3">
    <div class="container" id="pricing">
        <div class=" bottom-head text-center">
            <h2><a href="index.html">Pricing</a></h2>
            <div class="buttom-para">
                <p>We make your wedding day the best and most memorable one</p>
                <p>Our pricing are all affordable</p>
            </div>
        </div>

        -->	<!--Services-->
    </div>
    <div class="container">
        <h2 style="text-align:center">Our Pricing</h2>
        <p style="text-align:center">Our Pricing are affordable compare to what we give you on your most memorable occassion</p>

        <div class="columns w3-card">
            <ul class="price">
                <li class="header w3-card">Free</li>
                <li class="grey">&#8358;0</li>
                <li>Unlimited Photos</li>
                <li>500 Sms Units/Unlimited</li>
                <li>>com.ng domain name</li>
                <li>Gift Collection</li>
                <li class="grey">
                    <form method="post" action="{{route('plan.choose')}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="plan" value="1">
                        <button class="button">Choose Plan</button>
                    </form>
                </li>
            </ul>
        </div>

        <div class="columns w3-card">
            <ul class="price">
                <li class="header w3-card">Premium</li>
                <li class="grey">&#8358;80,000</li>
                <li>Unlimited Photos</li>
                <li>500 Sms Units/Unlimited</li>
                <li>>com.ng domain name</li>
                <li>Gift Collection</li>
                <li class="grey">
                    <form method="post" action="{{route('plan.choose')}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="plan" value="2">
                        <button class="button">Choose Plan</button>
                    </form>
                </li>
            </ul>
        </div>

        <div class="columns w3-card">
            <ul class="price">
                <li class="header">Custom</li>
                <li class="grey">Call Us</li>
                <li>Unlimited Photos</li>
                <li>100 Sms Units</li>
                <li>>com.ng domain name</li>
                <li>Gift Collection</li>
                <li class="grey">
                    <form method="post" action="{{route('plan.choose')}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="plan" value="3">
                        <button class="button">Choose Plan</button>
                    </form>
                </li>
            </ul>
        </div>
    </div>

    <div class="services" id="services" style="margin-top: 30px;">
        <div class="container">
            <h3 class="title clr">Services</h3>

            <div class="banner-bottom-girds text-center">
                <div class="col-md-6  col-sm-6 col-xs-6  its-banner-grid">
                    <div class="white-shadow">
                        <span class="fa fa-file-text banner-icon" aria-hidden="true"></span>
                        <h4>Online Invitation</h4>
                        <p>delectus reiciendis maiores alias consequatur aut.maiores alias consequatur aut.</p>
                    </div>
                </div>
                <div class="col-md-6  col-sm-6 col-xs-6 its-banner-grid">
                    <div class="white-shadow">
                        <span class="fa fa-address-book banner-icon" aria-hidden="true"></span>
                        <h4>Guest Book</h4>
                        <p>delectus reiciendis maiores alias consequatur aut.maiores alias consequatur aut.</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6  its-banner-grid">
                    <div class="white-shadow">
                        <span class="fa fa-music banner-icon" aria-hidden="true"></span>
                        <h4>Entertainment</h4>
                        <p>delectus reiciendis maiores alias consequatur aut.maiores alias consequatur aut.</p>
                    </div>
                </div>
                <div class="col-md-6  col-sm-6 col-xs-6  clr1 its-banner-grid">
                    <div class="white-shadow">
                        <span class="fa  fa-heart  banner-icon" aria-hidden="true"></span>
                        <h4>Love</h4>
                        <p> delectus reiciendis maiores alias consequatur aut.maiores alias consequatur aut.</p>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>

    <!--//Services-->


    <div class=" text-center" style="margin: 20px auto -30px auto; ">
        <div class="post">
            <form action="#" method="post">

                <div class="letter">
                    <input class="email" type="email" placeholder="Your email..." required="">
                </div>
                <div class="newsletter">
                    <input type="submit" value="Subscribe">
                </div>
            </form>
        </div>
    </div>
</div>



<footer>
    <div class=" copyright text-center">
        <div class="icons" >
            <ul>
                <li><a href="#"><span class="fa fa-facebook" style="color: white;"></span></a></li>
                <li><a href="#"><span class="fa fa-twitter" style="color: white;"></span></a></li>
                <li><a href="#"><span class="fa fa-rss" style="color: white;"></span></a></li>
                <li><a href="#"><span class="fa fa-vk" style="color: white;"></span></a></li>
            </ul>
        </div>

    </div>
    <p>  &copy; 2018 Weddingday.com.ng All Rights Reserved </p>
</footer>


<!--js working-->
<script type='text/javascript' src='{{asset('assets/js/jquery-2.2.3.min.js')}}'></script>
<script src="{{asset('assets/js/bootstrap.js')}}"></script>
<!-- //js  working-->

<!--  light box js -->
<script src="{{asset('assets/js/lightbox-plus-jquery.min.js')}}"> </script>
<!-- //light box js-->
<!-- banner-->
<script src="{{asset('assets/js/responsiveslides.min.js')}}"></script>
<script>
    $(window).load(function() {
        $('.preloader').fadeOut('slow');
    });
</script>
<script>
    $(function () {
        $("#slider").responsiveSlides({
            auto: true,
            pager: true,
            nav: true,
            speed: 1000,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });
    });
</script>
<!--// banner-->
<!--scripts-->
<script src="{{asset('assets/js/jquery.countdown.js')}}"></script>
<!--countdowntimer-js-->
<script src="{{asset('assets/js/script.js')}}"></script>
<!--countdowntimer-js-->
<!--//scripts-->

<!-- start-smoth-scrolling -->
<script type="text/javascript" src="{{asset('assets/js/move-top.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/easing.js')}}"></script>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(".scroll").click(function (event) {
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $(this.hash).offset().top
            }, 900);
        });
    });
</script>
<!-- start-smoth-scrolling -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function () {

        var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
        };


        $().UItoTop({
            easingType: 'easeOutQuart'
        });

    });
</script>
<!-- //here ends scrolling icon -->
</body>

</html>