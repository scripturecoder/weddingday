<!DOCTYPE html>
<html>

<head>
    <title>Wedding Day | Home</title>

    <!--meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Weddingday,
wedding website, Wedding day" />
    <script type="application/x-javascript">
        addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); }
    </script>
    <!--//meta tags ends here-->

    <!--booststrap-->

    <link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all">
    <!--//booststrap end-->

    <!-- font-awesome icons -->
    <link href="{{asset('assets/css/font-awesome.css')}}" rel="stylesheet">
    <!-- //font-awesome icons -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/w3.css')}}">

    <!-- banner text slider-->

    <!--// banner text slider-->

    <!--jquery-css counter time-->
    <link rel="stylesheet" href="{{asset('assets/css/jquery.countdown.css')}}" />
    <!--//jquery-css counter time-->

    <!--lightbox slider-->
    <link rel="stylesheet" href="{{asset('assets/css/lightbox.css')}}">
    <!-- lightbox slider-->

    <!--stylesheets-->
    <link href="{{asset('assets/css/style2.css')}}" rel='stylesheet' type='text/css' media="all">
    <!--//stylesheets-->
    <link href="//fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
<div class="banner">

    <div class="header-w3layouts">
        <!-- Navigation -->

        <div class="container">
            <div class="header-bar">
                <nav class="navbar navbar-default">
                    <div class="navbar-header navbar-left">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <h1><a class="navbar-brand" href="index.html">Wedding<span>Day</span></a></h1>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                        <nav>
                            <ul class="nav navbar-nav">
                                <li><a href="#about" class="scroll">About</a></li>
                                <li><a href="#services" class="scroll">Services</a></li>
                                <li><a href="#pricing" class="scroll">Pricing</a></li>
                                <li><a href="#contact" class="scroll">Contact</a></li>


                                @if(auth()->check())
                                    {{-- @if (auth()->user()->role === 1)
                                         <a class="w3-bar-item" href="/ddashboard">Dashboard</a>
                                     @elseif (auth()->user()->role === 2)
                                         <a class="w3-bar-item" href="/cdashboard">Dashboard</a>
                                     @elseif (auth()->user()->role === 3)
                                         <a class="w3-bar-item" href="/admin">Dashboard</a>
                                     @endif--}}
                                    <li><a href="/logout">Logout</a></li>
                                    <li><a href="/dashboard">Dashboard</a></li>
                                @else
                                    <li><a href="/register">Signup</a></li>
                                    <li><a href="/login">Signin</a></li>
                                @endif

                            </ul>
                        </nav>



                    </div>
                </nav>
            </div>

            <div class="clearfix"> </div>

        </div>

    </div>
    <div class="container">

        <!--Slider-->
        <div class="slider">
            <div class="callbacks_container w3l">
                <ul class="rslides" id="slider">
                    <li>

                        <div class="slider-info" id="takefriends">
                            <h4>Take your friends along on the Journey of a lifetime..</h4>


                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</div>


<div class="clearfix"> </div>
<div class=" slider-photo">
    <div class="container">
        <h3 class="title clr">Choose a Design</h3>

        <div class="w3-row-padding">
            @foreach($designs as $design)
                <div class="w3-third w3-margin-bottom w3-display-container">
                    <img id="img" onclick="onClick(this)" src="{{asset('images/designs/'.$design->photo)}}" alt="Image" style="width:100%; height: 250px; cursor: pointer;">
                    <div class="w3-display-middle w3-display-hover w3-xlarge">
                        <form method="post" action="{{route('design.choose')}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="design" value="{{$design->id}}">
                            @if(auth()->check())
                                <button class="w3-button w3-green w3-display-middle" style=" font-size:12px;">Choose</button>
                            @else
                                <a class="w3-button w3-green w3-display-middle" style=" font-size:12px;">Choose</a>
                            @endif  ?
                        </form>
                        <button id="id" class="w3-button w3-red w3-display-middle" style="margin-top: 40px; font-size: 12px;" onclick="document.getElementById('mod2').style.display='block'">Preview</button>
                    </div>
                </div>
            @endforeach
        </div>




        <div class=" copyright text-center">
            <div class="icons">
                <ul>
                    <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                    <li><a href="#"><span class="fa fa-rss"></span></a></li>
                    <li><a href="#"><span class="fa fa-vk"></span></a></li>
                </ul>
            </div>
        </div>
    </div>

            <!-- The Modal -->
            <div id="id01" class="w3-modal">
                <div class="w3-modal-content w3-round-large" style="max-width: 450px">

                    <div id="London" class="w3-container city">
                        <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                        <div class="icons w3-margin-top w3-center">
                            <span class="fa fa-lock" style="font-size: 75px; padding: 10px 15px 10px 15px; color: gray;" ></span>
                        </div>

                        <form class="w3-margin-bottom" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <label class="w3-margin-top">Email</label>
                            <input type="email" name="email" value="{{ old('email') }}" required autofocus class="w3-input w3-border">
                            <label class="w3-margin-top">Password</label>
                            <input type="password" name="password" required class="w3-input w3-border">
                            <h5>Don't Have an account?<a onclick="openCity('Paris')" style="color: blue;">Signup</a></h5><button class="btn btn-primary w3-margin-top w3-margin-bottom w3-right">Login</button>
                        </form><br>

                        <h4 class="w3-center w3-margin-top">OR</h4>

                        <div class="icons w3-margin-top w3-margin-bottom w3-center">
                            <a href="#"><span class="fa fa-facebook w3-dark-gray w3-hover-black w3-circle" style="font-size: 25px; padding: 10px 15px 10px 15px"></span></a>
                            <a href="#"><span class="fa fa-twitter w3-dark-gray w3-hover-black w3-circle" style="font-size: 25px; padding: 10px;"></span></a>
                            <a href="#"><span class="fa fa-github w3-dark-gray w3-hover-black w3-circle" style="font-size: 25px; padding: 10px;"></span></a>
                            <a href="#"><span class="fa fa-google w3-dark-gray w3-hover-black w3-circle" style="font-size: 25px; padding: 10px;"></span></a>
                        </div>

                    </div>

                    <div id="si" class="w3-container tab" style="display:none">
                        <h4 class="w3-margin-top w3-center">Signup in Few Minutes</h4>
                        <form class="w3-margin-top w3-margin-bottom" method="POST" action="{{ route('registration') }}">
                            {{ csrf_field() }}
                            <label class="w3-margin-top">Full Name	</label>
                            <input type="text" name="name" value="{{ old('name') }}"  class="w3-input w3-border">
                            <label class="w3-margin-top">Phone	</label>
                            <input type="number" name="phone" value="{{ old('phone') }}" class="w3-input w3-border">

                            <label class="w3-margin-top">Email</label>
                            <input type="email" class="w3-input w3-border" name="email" value="{{ old('email') }}">
                            <label class="w3-margin-top" >Password</label>
                            <input type="password" class="w3-input w3-border" name="password">
                            <label class="w3-margin-top">Confirm Password</label>
                            <input type="password" class="w3-input w3-border" name="password_confirmation">
                            <button class="btn btn-primary w3-margin-top w3-margin-bottom w3-right">Signup</button>

                            <h3>Have an account?<a onclick="openCity('London')" style="color: blue;">Login</a></h3>
                        </form><br>

                        <h4 class="w3-center w3-margin-top">OR</h4>

                        <div class="icons w3-margin-top w3-margin-bottom w3-center">
                            <a href="#"><span class="fa fa-facebook w3-dark-gray w3-hover-black w3-circle" style="font-size: 25px; padding: 10px 15px 10px 15px"></span></a>
                            <a href="#"><span class="fa fa-twitter w3-dark-gray w3-hover-black w3-circle" style="font-size: 25px; padding: 10px;"></span></a>
                            <a href="#"><span class="fa fa-github w3-dark-gray w3-hover-black w3-circle" style="font-size: 25px; padding: 10px;"></span></a>
                            <a href="#"><span class="fa fa-google w3-dark-gray w3-hover-black w3-circle" style="font-size: 25px; padding: 10px;"></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <footer>
                <p>  &copy; 2018 Weddingday.com.ng All Rights Reserved </p>
            </footer>
            <!--This are my image display modals -->
            <div id="modal01" class="w3-modal" onclick="this.style.display='none'">
                <span class="w3-button w3-hover-red w3-xlarge w3-display-topright">&times;</span>
                <div class="w3-modal-content w3-animate-zoom">
                    <img id="img01" style="width:100%">
                </div>
            </div>

            <script>
                function onClick(element) {
                    document.getElementById("img01").src = element.src;
                    document.getElementById("modal01").style.display = "block";
                }
            </script>


            <script>
                function openCity(cityName) {
                    var i;
                    var x = document.getElementsByClassName("tab");
                    for (i = 0; i < x.length; i++) {
                        x[i].style.display = "none";
                    }
                    document.getElementById(cityName).style.display = "block";
                }
            </script>

            <!--js working-->
            <script type='text/javascript' src='{{asset('assets/js/jquery-2.2.3.min.js')}}'></script>
            <script src="{{asset('assets/js/bootstrap.js')}}"></script>
            <!-- //js  working-->

            <!--  light box js -->
            <script src="{{asset('assets/js/lightbox-plus-jquery.min.js')}}"> </script>
            <!-- //light box js-->
            <!-- banner-->
            <script src="{{asset('assets/js/responsiveslides.min.js')}}"></script>
            <script>
                $(function () {
                    $("#slider").responsiveSlides({
                        auto: true,
                        pager: true,
                        nav: true,
                        speed: 1000,
                        namespace: "callbacks",
                        before: function () {
                            $('.events').append("<li>before event fired.</li>");
                        },
                        after: function () {
                            $('.events').append("<li>after event fired.</li>");
                        }
                    });
                });
            </script>
            <!--// banner-->
            <!--scripts-->
            <script src="{{asset('assets/js/jquery.countdown.js')}}"></script>
            <!--countdowntimer-js-->
            <script src="{{asset('assets/js/script.js')}}"></script>
            <!--countdowntimer-js-->
            <!--//scripts-->

            <!-- start-smoth-scrolling -->
            <script type="text/javascript" src="{{asset('assets/js/move-top.js')}}"></script>
            <script type="text/javascript" src="{{asset('assets/js/easing.js')}}"></script>
            <script type="text/javascript">
                jQuery(document).ready(function ($) {
                    $(".scroll").click(function (event) {
                        event.preventDefault();
                        $('html,body').animate({
                            scrollTop: $(this.hash).offset().top
                        }, 900);
                    });
                });
            </script>
            <!-- start-smoth-scrolling -->
            <!-- here stars scrolling icon -->
            <script type="text/javascript">
                $(document).ready(function () {

                    var defaults = {
                        containerID: 'toTop', // fading element id
                        containerHoverID: 'toTopHover', // fading element hover id
                        scrollSpeed: 1200,
                        easingType: 'linear'
                    };


                    $().UItoTop({
                        easingType: 'easeOutQuart'
                    });

                    $("#id").click(function(){
                        $("#img").click();
                    });
                });


            </script>
</div>       <!-- //here ends scrolling icon -->
</body>

</html>